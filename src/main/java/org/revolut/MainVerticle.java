package org.revolut;

import io.reactivex.Single;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import org.revolut.db.DatabaseVerticle;
import org.revolut.rest.HttpServerVerticle;

public class MainVerticle extends AbstractVerticle {
    @Override
    public void start(Future<Void> startFuture) {

        Single<String> dbVerticleDeployment = vertx.rxDeployVerticle(DatabaseVerticle.class.getName());

        dbVerticleDeployment
            .flatMap(
                id -> vertx.rxDeployVerticle(HttpServerVerticle.class.getName(),
                    new DeploymentOptions().setInstances(1))
            )
            .subscribe(
                id -> startFuture.complete(),
                startFuture::fail
            );
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        DeploymentOptions options = new DeploymentOptions()
            .setConfig(new JsonObject()
                .put("http.port", 8080)
                .put("infinispan.host", "localhost")
            );
        vertx.deployVerticle(MainVerticle.class, options);
    }
}
