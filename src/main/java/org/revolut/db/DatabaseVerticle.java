package org.revolut.db;

import io.reactivex.Completable;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import io.vertx.serviceproxy.ServiceBinder;
import org.flywaydb.core.Flyway;
import org.revolut.db.service.AccountService;
import org.revolut.db.service.UserService;
import org.revolut.db.service.UserTransactionService;

public class DatabaseVerticle extends AbstractVerticle {

    private DatasourceConfig datasourceConfig;
    private JDBCClient dbClient;

    @Override
    public void start(Future<Void> future) {
        datasourceConfig = new DatasourceConfig(config().getJsonObject("datasource", new JsonObject()));
        dbClient = JDBCClient.createShared(vertx, datasourceConfig.toJson(), "ModemGW");

        updateDB();

        updateDB()
            .andThen(registerServices())
            .subscribe(future::complete, future::fail);

    }

    private Completable updateDB() {
        return Completable.create(a -> {
            Flyway flyway = new Flyway();
            flyway.setDataSource(datasourceConfig.getUrl(), datasourceConfig.getUser(), datasourceConfig.getPassword());
            flyway.migrate();
            a.onComplete();
        });
    }

    private Completable registerServices() {
        return Completable.create(a -> {
            ServiceBinder binder = new ServiceBinder(vertx.getDelegate());
            registerAccountService(binder);
            registerUserService(binder);
            registerUserTransaction(binder);
            a.onComplete();
        });
    }

    private void registerUserService(ServiceBinder binder) {
        binder.setAddress(UserService.ADDRESS).register(UserService.class, UserService.create(dbClient));
    }

    private void registerAccountService(ServiceBinder binder) {
        binder.setAddress(AccountService.ADDRESS).register(AccountService.class, AccountService.create(dbClient));
    }

    private void registerUserTransaction(ServiceBinder binder){
        binder.setAddress(UserTransactionService.ADDRESS).register(UserTransactionService.class, UserTransactionService.create(vertx, dbClient));
    }
}
