package org.revolut.db;

import io.vertx.core.json.JsonObject;

public class DatasourceConfig {

    private final String url;
    private final String user;
    private final String password;

    public DatasourceConfig(JsonObject datasourceConfig) {
        url = datasourceConfig.getString("url", "jdbc:h2:mem:test;MODE=POSTGRESQL;DB_CLOSE_DELAY=-1");
        user = datasourceConfig.getString("user", "money");
        password = datasourceConfig.getString("password", "money");
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public JsonObject toJson() {
        return new JsonObject()
            .put("url", url)
            .put("user", user)
            .put("password", password)
            .put("driver_class", "org.h2.Driver");
    }
}
