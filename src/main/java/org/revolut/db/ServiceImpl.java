package org.revolut.db;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.CompletableHelper;
import io.vertx.reactivex.SingleHelper;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import org.revolut.db.model.Entity;

import java.util.ArrayList;
import java.util.List;

public abstract class ServiceImpl<T extends Entity> {
    private static final Logger LOG = LoggerFactory.getLogger(DatabaseVerticle.class);
    protected static final long LOCK_TIMEOUT = 10000;

    private final JDBCClient dbClient;
    private final Function<? super JsonArray, ? extends T> jsonMapper;
    private final Function<? super T, ? extends JsonArray> itemMapper;

    public ServiceImpl(
        JDBCClient dbClient,
        Function<? super JsonArray, ? extends T> jsonMapper,
        Function<? super T, ? extends JsonArray> itemMapper
    ) {
        this.dbClient = dbClient;
        this.jsonMapper = jsonMapper;
        this.itemMapper = itemMapper;
    }

    protected void findAll(Handler<AsyncResult<List<T>>> resultHandler, String query) {
        dbClient.rxQuery(query)
            .flatMapPublisher(
                res -> {
                    List<JsonArray> results = res.getResults();
                    return Flowable.fromIterable(results);
                }).map(jsonMapper)
            .collect(ArrayList<T>::new, ArrayList::add)
            .subscribe(SingleHelper.toObserver(resultHandler));
    }

    protected void findById(Long id, Handler<AsyncResult<T>> resultHandler, String query) {
        dbClient.rxQuerySingleWithParams(query, new JsonArray().add(id))
            .map(jsonMapper)
            .subscribe(SingleHelper.toObserver(resultHandler));
    }

    protected void save(T item, Handler<AsyncResult<Void>> resultHandler, String insertQuery, String updateQuery) {
        JsonArray json = null;
        try {
            json = itemMapper.apply(item);
        } catch (Exception e) {
            LOG.error(e);
        }
        if (item.getId() != null) {
            dbClient.rxUpdateWithParams(updateQuery, json)
                .toCompletable()
                .subscribe(CompletableHelper.toObserver(resultHandler));
        } else {
            dbClient.rxUpdateWithParams(insertQuery, json)
                .toCompletable()
                .subscribe(CompletableHelper.toObserver(resultHandler));
        }
    }
}
