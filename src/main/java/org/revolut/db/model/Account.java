package org.revolut.db.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.Objects;

@DataObject
public class Account implements Entity {

    @JsonProperty
    private long id;

    @JsonProperty(required = true)
    private Long userId;

    @JsonProperty(required = true)
    private Double balance;

    @JsonProperty(required = true)
    private String currencyCode;

    public Account() {
    }

    public Account(JsonObject json) {
        id = json.getLong("id");
        userId = json.getLong("userId");
        balance = json.getDouble("balance");
        currencyCode = json.getString("currencyCode");
    }

    public Account(Long userId, Double balance, String currencyCode) {
        this.userId = userId;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public Account(long id, Long userId, Double balance, String currencyCode) {
        this.id = id;
        this.userId = userId;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public Double getBalance() {
        return balance;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Account id(Long id) {
        this.id = id;
        return this;
    }

    public Account userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Account balance(Double balance) {
        this.balance = balance;
        return this;
    }

    public Account currencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }
        Account other = (Account) o;
        return Objects.equals(id, other.id) &&
            Objects.equals(userId, other.userId) &&
            Objects.equals(balance, other.balance) &&
            Objects.equals(currencyCode, other.currencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, balance, currencyCode);
    }

    @Override
    public String toString() {
        return "Account{" +
            "id=" + id +
            ", userId='" + userId + '\'' +
            ", balance=" + balance +
            ", currencyCode='" + currencyCode + '\'' +
            ", currencyCode='" + currencyCode + '\'' +
            '}';
    }

    public JsonObject toJson() {
        return new JsonObject()
            .put("id", id)
            .put("userId", userId)
            .put("balance", balance)
            .put("currencyCode", currencyCode);
    }
}
