package org.revolut.db.model;

public interface Entity {
    Long getId();
}
