package org.revolut.db.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.Objects;

@DataObject
public class UserTransaction implements Entity {
    @JsonProperty(required = true)
    private Long id;

    @JsonProperty(required = true)
    private String currencyCode;

    @JsonProperty(required = true)
    private Double amount;

    @JsonProperty(required = true)
    private Long fromAccountId;

    @JsonProperty(required = true)
    private Long toAccountId;

    public UserTransaction() {
    }

    public UserTransaction(JsonObject json) {
        id = json.getLong("id");
        currencyCode = json.getString("currencyCode");
        amount = json.getDouble("amount");
        fromAccountId = json.getLong("fromAccountId");
        toAccountId = json.getLong("toAccountId");
    }

    public UserTransaction(String currencyCode, Double amount, Long fromAccountId, Long toAccountId) {
        this.currencyCode = currencyCode;
        this.amount = amount;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Double getAmount() {
        return amount;
    }

    public Long getFromAccountId() {
        return fromAccountId;
    }

    public Long getToAccountId() {
        return toAccountId;
    }

    public UserTransaction id(Long id) {
        this.id = id;
        return this;
    }

    public UserTransaction currencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public UserTransaction amount(Double amount) {
        this.amount = amount;
        return this;
    }

    public UserTransaction fromAccountId(Long fromAccountId) {
        this.fromAccountId = fromAccountId;
        return this;
    }

    public UserTransaction toAccountId(Long toAccountId) {
        this.toAccountId = toAccountId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof UserTransaction)) {
            return false;
        }
        UserTransaction other = (UserTransaction) o;
        return Objects.equals(id, other.id) &&
            Objects.equals(currencyCode, other.currencyCode) &&
            Objects.equals(amount, other.amount) &&
            Objects.equals(fromAccountId, other.fromAccountId) &&
            Objects.equals(toAccountId, other.toAccountId);

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, currencyCode, amount, fromAccountId, toAccountId);
    }

    @Override
    public String toString() {
        return "UserTransaction{" + "currencyCode='" + currencyCode + '\'' + ", amount=" + amount + ", fromAccountId="
            + fromAccountId + ", toAccountId=" + toAccountId + '}';
    }

    public JsonObject toJson() {
        return new JsonObject()
            .put("currencyCode", currencyCode)
            .put("amount", amount)
            .put("fromAccountId", fromAccountId)
            .put("toAccountId", toAccountId);
    }
}
