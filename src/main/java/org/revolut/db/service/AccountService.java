package org.revolut.db.service;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import org.revolut.db.model.Account;

import java.util.List;

@ProxyGen
@VertxGen
public interface AccountService {

    String ADDRESS = "vertx.account";

    @GenIgnore
    static AccountService create(JDBCClient dbClient) {
        return new AccountServiceImpl(dbClient);
    }

    @Fluent
    AccountService findById(Long id, Handler<AsyncResult<Account>> handler);

    @Fluent
    AccountService findAll(Handler<AsyncResult<List<Account>>> handler);

    @Fluent
    AccountService save(Account account, Handler<AsyncResult<Void>> handler);
}
