package org.revolut.db.service;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import org.revolut.db.ServiceImpl;
import org.revolut.db.model.Account;

import java.util.List;

public class AccountServiceImpl extends ServiceImpl<Account> implements AccountService {

    private static final String FIND_ALL_QUERY = "SELECT a.id, a.user_id, a.balance, a.currency_code FROM accounts a";
    private static final String BY_ID_QUERY = "SELECT a.id, a.user_id, a.balance, a.currency_code FROM accounts a WHERE a.id = ?";
    private static final String INSERT_QUERY = "INSERT INTO accounts(user_id, balance, currency_code) VALUES (?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE accounts SET user_id = ?2, balance = ?3, currency_code = ?4 WHERE id = ?1";

    AccountServiceImpl(JDBCClient dbClient) {
        super(dbClient, AccountServiceImpl::map, AccountServiceImpl::map);
    }

    @Override
    public AccountService findById(Long id, Handler<AsyncResult<Account>> handler) {
        findById(id, handler, BY_ID_QUERY);
        return this;
    }

    @Override
    public AccountService findAll(Handler<AsyncResult<List<Account>>> handler) {
        findAll(handler, FIND_ALL_QUERY);
        return this;
    }

    @Override
    public AccountService save(Account account, Handler<AsyncResult<Void>> handler) {
        save(account, handler, INSERT_QUERY, UPDATE_QUERY);
        return this;
    }

    private static Account map(JsonArray arr) {
        return new Account()
            .id(arr.getLong(0))
            .userId(arr.getLong(1))
            .balance(arr.getDouble(2))
            .currencyCode(arr.getString(3));
    }

    private static JsonArray map(Account json) {
        return new JsonArray()
            .add(json.getId())
            .add(json.getUserId())
            .add(json.getBalance())
            .add(json.getCurrencyCode());
    }
}
