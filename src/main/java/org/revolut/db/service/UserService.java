package org.revolut.db.service;


import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import org.revolut.db.model.User;

import java.util.List;

@ProxyGen
@VertxGen
public interface UserService {
    String ADDRESS = "vertx.user";

    @GenIgnore
    static UserService create(JDBCClient dbClient) {
        return new UserServiceImpl(dbClient);
    }

    @Fluent
    UserService findById(Long id, Handler<AsyncResult<User>> handler);

    @Fluent
    UserService findAll(Handler<AsyncResult<List<User>>> handler);
}
