package org.revolut.db.service;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import org.revolut.db.ServiceImpl;
import org.revolut.db.model.User;

import java.util.List;

public class UserServiceImpl extends ServiceImpl<User> implements UserService {

    private static final String FIND_ALL_QUERY = "SELECT u.id, u.name, u.email FROM users u";
    private static final String BY_ID_QUERY = "SELECT u.id, u.name, u.email FROM users u WHERE u.id = ?";

    UserServiceImpl(JDBCClient dbClient){
        super(dbClient, UserServiceImpl::map, UserServiceImpl::map);
    }

    @Override
    public UserService findById(Long id, Handler<AsyncResult<User>> handler) {
        findById(id, handler, BY_ID_QUERY);
        return this;
    }

    @Override
    public UserService findAll(Handler<AsyncResult<List<User>>> handler) {
        findAll(handler, FIND_ALL_QUERY);
        return this;
    }

    private static User map(JsonArray arr) {
        return new User()
            .id(arr.getLong(0))
            .name(arr.getString(1))
            .emailAddress(arr.getString(2));
    }

    private static JsonArray map(User json) {
        return new JsonArray()
            .add(json.getId())
            .add(json.getName())
            .add(json.getEmail());
    }
}
