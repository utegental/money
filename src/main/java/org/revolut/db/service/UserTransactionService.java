package org.revolut.db.service;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import org.revolut.db.model.UserTransaction;

@ProxyGen
@VertxGen
public interface UserTransactionService {

    String ADDRESS = "vertx.userTransaction";

    @GenIgnore
    static UserTransactionService create(Vertx vertx, JDBCClient dbClient) {
        return new UserTransactionServiceImpl(vertx, dbClient);
    }

    @Fluent
    UserTransactionService save(UserTransaction transaction, Handler<AsyncResult<Void>> handler);
}
