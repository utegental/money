package org.revolut.db.service;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.UpdateResult;
import io.vertx.reactivex.CompletableHelper;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import io.vertx.reactivex.ext.sql.SQLConnection;
import org.revolut.db.ServiceImpl;
import org.revolut.db.model.UserTransaction;

import java.util.Objects;

public class UserTransactionServiceImpl extends ServiceImpl<UserTransaction> implements UserTransactionService {

    private static final String INSERT_QUERY = "INSERT INTO transaction(currency_code, amount, from_account_id, to_account_id) VALUES (?, ?, ?, ?)";
    private static final String UPDATE_ACC_QUERY = "UPDATE accounts SET balance = ?2 WHERE id = ?1";
    private static final String SELECT_BALANCE_QUERY = "SELECT a.balance FROM accounts a where a.id = ?";
    private static final String SELECT_CCY_QUERY = "SELECT a.currency_code FROM accounts a where a.id = ?";

    private final Vertx vertx;
    private final JDBCClient dbClient;

    UserTransactionServiceImpl(Vertx vertx, JDBCClient dbClient) {
        super(dbClient, UserTransactionServiceImpl::map, UserTransactionServiceImpl::map);
        this.vertx = vertx;
        this.dbClient = dbClient;
    }

    @Override
    public UserTransactionService save(UserTransaction transaction, Handler<AsyncResult<Void>> handler) {
        saveWithLock(transaction).subscribe(CompletableHelper.toObserver(handler));
        return this;
    }

    private Completable saveWithLock(UserTransaction transaction) {
        Long first = Math.min(transaction.getFromAccountId(), transaction.getToAccountId());
        Long second = Math.max(transaction.getFromAccountId(), transaction.getToAccountId());

        return vertx.sharedData().rxGetLockWithTimeout("account:" + first, LOCK_TIMEOUT)
            .flatMapCompletable(
                lock1 -> vertx.sharedData()
                    .rxGetLockWithTimeout("account:" + second, LOCK_TIMEOUT)
                    .flatMapCompletable(
                        lock2 -> save(transaction)
                            .doFinally(lock2::release)
                    ).doFinally(lock1::release)
            );
    }

    private Completable save(UserTransaction transaction) {
        Long fromId = transaction.getFromAccountId();
        Long toId = transaction.getToAccountId();
        Double amount = transaction.getAmount();
        return dbClient.rxGetConnection().flatMapCompletable(
            connection -> connection.rxSetAutoCommit(false)
                .andThen(selectBalance(connection, fromId)
                    .doOnSuccess(value -> validateValue(value, amount, fromId)))
                .flatMapCompletable(
                    from -> selectBalance(connection, toId)
                        .flatMapCompletable(to -> validateAccountsCcy(transaction, connection)
                            .andThen(insertTransaction(connection, transaction).toCompletable())
                            .andThen(updateAccount(connection, fromId, from - amount))
                            .andThen(updateAccount(connection, toId, to + amount))
                            .andThen(connection.rxCommit())
                        ).doFinally(connection::close)
                ));
    }

    private void validateValue(Double fromBalance, Double amount, Long fromId) {
        if (fromBalance - amount < 0) {
            throw new RuntimeException("Not sufficient Fund for account: " + fromId);
        }
    }

    private Completable validateAccountsCcy(UserTransaction transaction, SQLConnection connection) {
        return selectCcy(connection, transaction.getFromAccountId()).flatMapCompletable(
            from -> selectCcy(connection, transaction.getToAccountId())
                .doOnSuccess(to -> validateCcy(from, to, transaction.getCurrencyCode()))
                .toCompletable()
        );
    }

    private void validateCcy(String fromCcy, String toCcy, String transactionCcy) {
        if (!Objects.equals(fromCcy, toCcy) || !Objects.equals(toCcy, transactionCcy)) {
            throw new RuntimeException(String.format("Different ccy: {from=%s, to=%s, transaction=%s", fromCcy, toCcy, transactionCcy));
        }
    }

    private Single<UpdateResult> insertTransaction(SQLConnection connection, UserTransaction transaction) {
        return connection.rxUpdateWithParams(INSERT_QUERY, map(transaction));
    }

    private Single<Double> selectBalance(SQLConnection connection, Long id) {
        return connection.rxQuerySingleWithParams(SELECT_BALANCE_QUERY, new JsonArray().add(id)).map(a -> a.getDouble(0));
    }

    private Single<String> selectCcy(SQLConnection connection, Long id) {
        return connection.rxQuerySingleWithParams(SELECT_CCY_QUERY, new JsonArray().add(id)).map(a -> a.getString(0));
    }

    private Completable updateAccount(SQLConnection connection, Long id, Double balance) {
        return connection.rxUpdateWithParams(UPDATE_ACC_QUERY, new JsonArray().add(id).add(balance)).toCompletable();
    }

    private static UserTransaction map(JsonArray arr) {
        return new UserTransaction()
            .id(arr.getLong(0))
            .currencyCode(arr.getString(1))
            .amount(arr.getDouble(2))
            .fromAccountId(arr.getLong(3))
            .toAccountId(arr.getLong(4));
    }

    private static JsonArray map(UserTransaction json) {
        return new JsonArray()
            .add(json.getCurrencyCode())
            .add(json.getAmount())
            .add(json.getFromAccountId())
            .add(json.getToAccountId());
    }
}
