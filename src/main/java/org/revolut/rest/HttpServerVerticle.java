package org.revolut.rest;

import io.reactivex.Completable;
import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import io.vertx.serviceproxy.ServiceProxyBuilder;
import org.revolut.MainVerticle;
import org.revolut.db.reactivex.service.AccountService;
import org.revolut.db.reactivex.service.UserService;
import org.revolut.db.reactivex.service.UserTransactionService;
import org.revolut.rest.handler.TransactionHandler;
import org.revolut.rest.handler.account.*;
import org.revolut.rest.handler.user.UserGetAllHandler;
import org.revolut.rest.handler.user.UserGetHandler;

public class HttpServerVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);
    private static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";

    private UserService userService;
    private AccountService accountService;
    private UserTransactionService userTransactionService;

    @Override
    public void start(Future<Void> startFuture) {
        ServiceProxyBuilder builder = new ServiceProxyBuilder(vertx.getDelegate());
        userService = bindUserService(builder);
        accountService = bindAccountService(builder);
        userTransactionService = bindUserTransactionService(builder);

        setupWebServer().subscribe(startFuture::complete, startFuture::fail);
    }

    private Completable setupWebServer() {
        int portNumber = config().getInteger(CONFIG_HTTP_SERVER_PORT, 8080);

        Router router = Router.router(vertx);

        setupAccountRoutes(router);
        setupUserRoutes(router);

        return vertx.createHttpServer()
            .requestHandler(router::accept)
            .rxListen(portNumber)
            .doAfterSuccess(a -> LOGGER.info("HTTP server running on port " + portNumber))
            .toCompletable();
    }

    private UserService bindUserService(ServiceProxyBuilder builder) {
        return UserService.newInstance(
            builder.setAddress(org.revolut.db.service.UserService.ADDRESS)
                .build(org.revolut.db.service.UserService.class)
        );
    }

    private AccountService bindAccountService(ServiceProxyBuilder builder) {
        return AccountService.newInstance(
            builder.setAddress(org.revolut.db.service.AccountService.ADDRESS)
                .build(org.revolut.db.service.AccountService.class)
        );
    }

    private UserTransactionService bindUserTransactionService(ServiceProxyBuilder builder) {
        return UserTransactionService.newInstance(
            builder.setAddress(org.revolut.db.service.UserTransactionService.ADDRESS)
                .build(org.revolut.db.service.UserTransactionService.class)
        );
    }

    private void setupAccountRoutes(Router router) {
        String prefix = "/account";

        router.route().handler(BodyHandler.create());

        router.get(prefix).handler(new AccountGetAllHandler(accountService));
        router.get(prefix + "/:id").handler(new AccountGetHandler(accountService));
        router.get(prefix + "/:id/balance").handler(new GetBalanceHandler(accountService));

        router.put(prefix + "/:id/deposit/:amount").handler(new DepositHandler(vertx, accountService));
        router.put(prefix + "/:id/withdraw/:amount").handler(new WithdrawHandler(vertx, accountService));
        router.post("/transaction").handler(new TransactionHandler(userTransactionService));
    }

    private void setupUserRoutes(Router router) {
        String prefix = "/user";

        router.get(prefix).handler(new UserGetAllHandler(userService));
        router.get(prefix + "/:id").handler(new UserGetHandler(userService));
    }
}
