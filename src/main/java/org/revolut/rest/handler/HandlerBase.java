package org.revolut.rest.handler;

import io.reactivex.Single;
import io.vertx.core.Handler;
import io.vertx.core.json.Json;
import io.vertx.reactivex.ext.web.RoutingContext;

import java.util.regex.Pattern;

public abstract class HandlerBase implements Handler<RoutingContext> {

    private static final Pattern LONG_PATTERN = Pattern.compile("(-)?\\d+");
    protected static final String CONTENT_TYPE = "content-type";
    protected static final String JSON_TYPE = "application/json; charset=utf-8";

    protected Long parseLongParam(String param) {
        return LONG_PATTERN.matcher(param).matches() ? Long.valueOf(param) : null;
    }

    protected  <T> void subscribeResponse(Single<T> supplier, RoutingContext rc) {
        supplier.subscribe(items -> rc.response()
            .putHeader("content-type", "application/json; charset=utf-8")
            .end(Json.encodePrettily(items)));
    }
}
