package org.revolut.rest.handler;

import io.vertx.core.Handler;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.revolut.db.model.UserTransaction;
import org.revolut.db.reactivex.service.UserTransactionService;

public class TransactionHandler extends HandlerBase implements Handler<RoutingContext> {

    private final UserTransactionService userTransactionService;

    public TransactionHandler(UserTransactionService userTransactionService) {
        this.userTransactionService = userTransactionService;
    }

    @Override
    public void handle(RoutingContext rc) {
        UserTransaction transaction = new UserTransaction(rc.getBodyAsJson());

        userTransactionService.rxSave(transaction)
            .subscribe(() -> rc.response().putHeader(CONTENT_TYPE, JSON_TYPE).end(),
                a -> rc.response().setStatusCode(500).end());
    }
}
