package org.revolut.rest.handler.account;

import io.vertx.core.Handler;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.revolut.db.reactivex.service.AccountService;
import org.revolut.rest.handler.HandlerBase;

public class AccountGetHandler extends HandlerBase implements Handler<RoutingContext> {

    private final AccountService accountService;

    public AccountGetHandler(AccountService accountService){
        this.accountService = accountService;
    }

    @Override
    public void handle(RoutingContext rc) {
        subscribeResponse(accountService.rxFindById(parseLongParam(rc.pathParam("id"))), rc);
    }
}
