package org.revolut.rest.handler.account;

import io.vertx.core.Handler;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.revolut.db.reactivex.service.AccountService;
import org.revolut.rest.handler.HandlerBase;

public class DepositHandler extends HandlerBase implements Handler<RoutingContext> {

    private static final long LOCK_TIMEOUT = 10000;

    private final Vertx vertx;
    private final AccountService accountService;

    public DepositHandler(Vertx vertx, AccountService accountService) {
        this.vertx = vertx;
        this.accountService = accountService;
    }

    @Override
    public void handle(RoutingContext rc) {
        Long id = parseLongParam(rc.pathParam("id"));

        vertx.sharedData().rxGetLockWithTimeout("account:" + id, LOCK_TIMEOUT)
            .flatMapCompletable(lock -> accountService.rxFindById(id)
                .flatMapCompletable(a -> {
                    Double balance = a.getBalance() + Double.valueOf(rc.pathParam("amount"));
                    if (balance < 0) {
                        throw new RuntimeException("Not sufficient Fund for account: " + id);
                    }
                    return accountService.rxSave(a.balance(balance))
                        .doFinally(lock::release);
                })).subscribe(() -> rc.response().putHeader(CONTENT_TYPE, JSON_TYPE).end(), rc::fail);
    }
}
