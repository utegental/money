package org.revolut.rest.handler.account;

import io.vertx.core.Handler;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.revolut.db.reactivex.service.AccountService;
import org.revolut.rest.handler.HandlerBase;

public class GetBalanceHandler extends HandlerBase implements Handler<RoutingContext> {

    private final AccountService accountService;

    public GetBalanceHandler(AccountService accountService){
        this.accountService = accountService;
    }

    @Override
    public void handle(RoutingContext rc) {
        accountService.rxFindById(parseLongParam(rc.pathParam("id"))).subscribe(
            account -> rc.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(String.valueOf(account.getBalance())));
    }
}
