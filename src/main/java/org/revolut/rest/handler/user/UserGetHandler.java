package org.revolut.rest.handler.user;

import io.vertx.core.Handler;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.revolut.db.reactivex.service.UserService;
import org.revolut.rest.handler.HandlerBase;

public class UserGetHandler extends HandlerBase implements Handler<RoutingContext> {

    private final UserService userService;

    public UserGetHandler(UserService userService){
        this.userService = userService;
    }

    @Override
    public void handle(RoutingContext rc) {
        subscribeResponse(userService.rxFindById(parseLongParam(rc.pathParam("id"))), rc);
    }
}
