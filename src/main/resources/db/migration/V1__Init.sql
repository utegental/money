create table users (
  id    SERIAL PRIMARY KEY,
  name  VARCHAR(100),
  email VARCHAR(246)
);

create table accounts (
  id            SERIAL PRIMARY KEY,
  user_id       BIGINT         NOT NULL REFERENCES users,
  currency_code VARCHAR(3)     NOT NULL,
  balance       DECIMAL(20, 3) NOT NULL
);

create table transaction (
  id              SERIAL PRIMARY KEY,
  currency_code   VARCHAR(3)     NOT NULL,
  amount          DECIMAL(20, 3) NOT NULL,
  from_account_id BIGINT         NOT NULL REFERENCES accounts,
  to_account_id   BIGINT         NOT NULL REFERENCES accounts
);