INSERT INTO users (id, name, email) VALUES (1, 'smith', 'smith@gmail.com');
INSERT INTO users (id, name, email) VALUES (2, 'john', 'john@gmail.com');
INSERT INTO users (id, name, email) VALUES (3, 'sara', 'sara@gmail.com');

INSERT INTO accounts (id, user_id, balance, currency_code) VALUES (1, 1, 100.0000, 'USD');
INSERT INTO accounts (id, user_id, balance, currency_code) VALUES (2, 2, 200.0000, 'USD');
INSERT INTO accounts (id, user_id, balance, currency_code) VALUES (3, 1, 500.0000, 'EUR');
INSERT INTO accounts (id, user_id, balance, currency_code) VALUES (4, 2, 500.0000, 'EUR');
INSERT INTO accounts (id, user_id, balance, currency_code) VALUES (5, 1, 500.0000, 'GBP');
INSERT INTO accounts (id, user_id, balance, currency_code) VALUES (6, 2, 500.0000, 'GBP');