package org.revolut;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.core.buffer.Buffer;
import io.vertx.reactivex.ext.web.client.HttpResponse;
import io.vertx.reactivex.ext.web.client.WebClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.revolut.db.model.User;
import org.revolut.db.model.UserTransaction;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("A fairly basic payment test")
@ExtendWith(VertxExtension.class)
class VerticleTest {

    private io.vertx.reactivex.core.Vertx rxVertx;
    private WebClientOptions webClientOptions = new WebClientOptions().setDefaultPort(8080);
    private WebClient client;

    @BeforeEach
    void setup(Vertx vertx, VertxTestContext context) {
        rxVertx = new io.vertx.reactivex.core.Vertx(vertx);
        client = WebClient.create(rxVertx, webClientOptions);
        rxVertx.rxDeployVerticle(MainVerticle.class.getName())
            .toCompletable()
            .subscribe(context::completeNow, context::failNow);
    }

    @AfterEach
    void tearDown(Vertx vertx) {
        vertx.close();
    }

    @Test
    @DisplayName("Get all method")
    void getAllAccount(VertxTestContext context) {
        WebClient client = WebClient.create(rxVertx, webClientOptions);

        client.get("/user").rxSend()
            .doOnSuccess(a->{
                assertStatusOk(a);
                assertThat(a.body()).isNotNull();
                JsonArray items = a.bodyAsJsonArray();
                assertThat(items).isNotNull().hasSize(3);
            })
            .subscribe(a -> context.completeNow(), context::failNow);
    }

    @Test
    void getByIdAccount(VertxTestContext context) {
        client.get("/user/1").rxSend()
            .doOnSuccess(a -> {
                assertStatusOk(a);
                assertThat(a.body()).isNotNull();
                User user = a.bodyAsJson(User.class);
                assertThat(user).isNotNull();
                assertThat(user.getName()).isEqualTo("smith");
            }).subscribe(a -> context.completeNow(), context::failNow);
    }

    @Test
    void testDeposit(VertxTestContext context) {
        client.put("/account/1/deposit/100").rxSend().subscribe(a -> {
            assertStatusOk(a);
            assertThat(a.body()).isNull();

            client.get("/account/1/balance").rxSend()
                .doOnSuccess(b -> {
                    assertStatusOk(b);
                    assertThat(Double.valueOf(b.bodyAsString())).isEqualTo(200);
                }).subscribe(b -> context.completeNow(), context::failNow);

        }, context::failNow);
    }

    @Test
    void testWithdrawSufficient(VertxTestContext context) {
        client.put("/account/2/withdraw/100").rxSend()
            .subscribe(a -> {
                assertThat(a.statusCode()).isEqualTo(200);
                assertThat(a.body()).isNull();

                client.get("/account/2/balance").rxSend()
                    .doOnSuccess(b -> {
                        assertStatusOk(b);
                        assertThat(Double.valueOf(b.bodyAsString())).isEqualTo(100);
                    }).subscribe(b -> context.completeNow(), context::failNow);
            }, context::failNow);
    }

    @Test
    void testWithdrawNotSufficient(VertxTestContext context) {
        client.put("/account/2/withdraw/1000").rxSend()
            .doOnSuccess(a -> assertThat(a.statusCode()).isEqualTo(500))
            .subscribe(a -> context.completeNow(), context::failNow);
    }

    @Test
    void testTransactionSufficient(VertxTestContext context) {
        UserTransaction transaction = new UserTransaction().currencyCode("EUR").fromAccountId(3L).toAccountId(4L).amount(10.);
        client.post("/transaction")
            .putHeader("content-type", "application/json; charset=utf-8")
            .rxSendJsonObject(transaction.toJson())
            .doOnSuccess(a -> assertThat(a.statusCode()).isEqualTo(200))
            .subscribe(a -> {
                context.completeNow();
            }, context::failNow);
    }

    @Test
    void testTransactionInsufficient(VertxTestContext context) {
        UserTransaction transaction = new UserTransaction().currencyCode("EUR").fromAccountId(3L).toAccountId(4L).amount(1000000.);
        client.post("/transaction")
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .rxSendJsonObject(transaction.toJson())
            .doOnSuccess(a -> assertThat(a.statusCode()).isEqualTo(500))
            .subscribe(a -> context.completeNow(), context::failNow);
    }

    @Test
    void testTransactionDifferentCcy(VertxTestContext context) {
        UserTransaction transaction = new UserTransaction().currencyCode("USD").fromAccountId(3L).toAccountId(4L).amount(10.);
        client.post("/transaction")
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .rxSendJsonObject(transaction.toJson())
            .doOnSuccess(a -> assertThat(a.statusCode()).isEqualTo(500))
            .subscribe(a -> context.completeNow(), context::failNow);
    }

    private void assertStatusOk(HttpResponse<Buffer> response) {
        assertThat(response.statusCode()).isEqualTo(200);
    }
}
